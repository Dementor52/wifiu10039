<?php

namespace app\lips;

class image{

    protected $imgType = false;
    protected $imageResource = false;

    /**
     * Konstruktor
     * @param mixed [default = false]$img
     */
    public function  __construct($image = false){
        if( !empty ($image) ){
            $this->loadImg($image);
        }        

    }

    /**
     * Lade die Bildinformationen
     * @param string $img
     */
    public function loadImg(string $img){
        $image = getimagesize($img);
            
       
            $this->imageType = !empty($image[2]) ? $image[2] :false;
            
            if($image[2]==IMAGETYPE_JPEG){
                $this -> imageResource = imagecreatefromjpeg($img);
            }
            elseif ($image[2]==IMAGETYPE_GIF){
                $this -> imageResource = imagecreatefromgif($img);
            }
            elseif ($image[2]==IMAGETYPE_PNG){
                $this -> imageResource = imagecreatefrompng($img);
            } 
            
            else {
                throw new \Exception("Dateiformat nicht erlaubt");           
            }
          
        }
   
        /**
         * fix width
         * @param int $width
         */

         public function fixwidth(int $newwidth){
             $origWidth = imagesx($this->imageResource);
             $origHeight = imagesy($this->imageResource);
             $newHeight = round($origHeight / $origWidth*$newwidth);
            $this ->resize($newwidth,$newHeight);
             
                 
         } 
         
         
         /**
          * resize
          *@param int $newwidth
          *@param int $newheigth
          */

          public function resize(int $newwidth,int $newHeight){
            $newImage = imagecreatetruecolor($newwidth,$newHeight);

             imagecolortransparent($newImage,imagecolorallocate($newImage,0,0,0));
             imagealphablending($newImage,false);
             imagesavealpha($newImage,true);

             imagecopyresampled($newImage,$this->imageResource,0,0,0,0,$newwidth,$newHeight,$origWidth,$origHeight);
          }

             
          /**
           * Bild wird geschpeichert
           *
           */
            public function save (string $file){
            if ( $this->imageType ==IMAGETYPE_JPEG ){
                imagejpeg($newImage,'testbild.jpg',80);
            }
          
            else if( $this->imageType ==IMAGETYPE_GIF ){
               imagejpeg($newImage,'testbild.gif',80);
        }
        else if( $this->imageType ==IMAGETYPE_PNG ){
           imagejpeg($newImage,'testbild.png',80);
           }  
    }
          

          /**
           * Erzeugt einen neuen Pad falls noch nicht vorhanden
           * @param string Pfad/Dateiname
           */

           protected function createPath(string $file,$permissions =0755){
               $pathinfo = pathinfo($file);
               $newpath(isset($pathinfo['extension'])) ? $pathinfo['dirname']:$file;
               if($newpath && !is_dir($newpath)){
                   mkdir($newpath,$permissions,true);
               }
           
           }
}


